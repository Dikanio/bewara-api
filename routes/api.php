<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api'], function() {    
    Route::group(['prefix' => 'auth', 'as' => 'auth.', 'namespace' => 'Auth'], function () {        
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('register', 'AuthController@register')->name('register');
        Route::post('forgot-password', 'AuthController@forgotPassword')->name('forgot-password');
    });
    Route::group(['prefix' => 'master', 'as' => 'master.', 'namespace' => 'Master'], function () {        
        Route::get('provinsi', 'MasterController@getProvinsi')->name('provinsi');
        Route::get('kota', 'MasterController@getKota')->name('kota');
        Route::get('kecamatan', 'MasterController@getKecamatan')->name('kecamatan');
        Route::get('desa', 'MasterController@getDesa')->name('desa');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('auth/logout', 'Auth\AuthController@logout')->name('auth.logout');

        Route::group(['prefix' => 'user', 'namespace' => 'User', 'as' => 'user.'], function(){
            Route::get('/{id}', 'UserController@show')->name('show');
            Route::put('/{id}', 'UserController@update')->name('update');
        });

        // Route::group(['prefix' => 'user', 'namespace' => 'User', 'as' => 'user.'], function(){
        //     Route::post('verify-email', 'UserController@verifyEmail')->name('verify-email');
        //     Route::post('fcm-token', 'UserController@fcmToken')->name('fcm-token');

        //     Route::group(['prefix' => 'profile', 'as' => 'profile.'], function(){
        //         Route::get('/', 'ProfileController@show')->name('show');
        //         Route::patch('/edit', 'ProfileController@update')->name('update-profile');
        //         Route::patch('/change-password', 'ProfileController@updatePassword')->name('update-password');
        //     });
        // });
    });

});
