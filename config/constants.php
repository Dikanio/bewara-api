<?php

return [
    'CODE' => [
        'SUCCESS' => 200,
        'FAILED' => 400,
        'UNAUTHORIZED' => 401,
        'UNPROCESSABLE' => 422,
        'SERVER_FAILED' => 500,
    ],

    
];
