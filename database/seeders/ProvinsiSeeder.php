<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use App\Models\Provinsi;

class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $json = File::get("database/data/provinces.json");
        $data = json_decode($json);
        foreach($data as $obj) {
            Provinsi::create(array(
                'id' => $obj->id,
                'name' => $obj->name,
                // 'latitude' => $obj->latitude,
                // 'longitude' => $obj->longitude
            ));
        }

    }
}
