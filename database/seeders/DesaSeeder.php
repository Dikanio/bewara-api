<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use App\Models\Desa;

class DesaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/villages.json");
        $data = json_decode($json);
        foreach($data as $obj) {
            Desa::create(array(
                'id' => $obj->id,
                'kecamatan_id' => $obj->district_id,
                'name' => $obj->name,                
            ));
        }        
    }
}
