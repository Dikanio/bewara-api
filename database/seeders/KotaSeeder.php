<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use App\Models\Kota;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/regencies.json");
        $data = json_decode($json);
        foreach($data as $obj) {
            Kota::create(array(
                'id' => $obj->id,
                'provinsi_id' => $obj->province_id,
                'name' => $obj->name,
                // 'latitude' => $obj->latitude,
                // 'longitude' => $obj->longitude
            ));
        }        
    }
}
