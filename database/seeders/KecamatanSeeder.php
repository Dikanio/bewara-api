<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use App\Models\Kecamatan;

class KecamatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/districts.json");
        $data = json_decode($json);
        foreach($data as $obj) {
            Kecamatan::create(array(
                'id' => $obj->id,
                'kota_id' => $obj->regency_id,
                'name' => $obj->name,
                // 'latitude' => $obj->latitude,
                // 'longitude' => $obj->longitude
            ));
        }        
    }
}
