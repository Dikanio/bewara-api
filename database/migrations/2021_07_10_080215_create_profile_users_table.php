<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('provinsi_id')->nullable();
            $table->unsignedInteger('kota_id')->nullable();
            $table->unsignedInteger('kecamatan_id')->nullable();
            $table->unsignedInteger('desa_id')->nullable();
            $table->string('nik');
            $table->string('no_kk');
            $table->string('address');
            $table->string('rt', 20);
            $table->string('rw', 20);
            $table->string('phone_number');
            $table->string('photo')->nullable();
            $table->string('file')->nullable();
            $table->timestamps();
            
            $table->index(["user_id"], 'fk_profile_user_users');
            $table->foreign('user_id', 'fk_profile_user_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            // $table->index(["provinsi_id"], 'fk_profile_user_provinsi');
            // $table->foreign('provinsi_id', 'fk_profile_user_provinsi')
            //     ->references('id')
            //     ->on('provinsi')
            //     ->onDelete('set null')
            //     ->onUpdate('cascade');

            // $table->index(["kota_id"], 'fk_profile_user_kota');
            // $table->foreign('kota_id', 'fk_profile_user_kota')
            //     ->references('id')->on('kota')
            //     ->onDelete('set null')
            //     ->onUpdate('cascade');

            // $table->index(["kecamatan_id"], 'fk_profile_user_kecamatan');
            // $table->foreign('kecamatan_id', 'fk_profile_user_kecamatan')
            //     ->references('id')
            //     ->on('kecamatan')
            //     ->onDelete('set null')
            //     ->onUpdate('cascade');

            // $table->index(["desa_id"], 'fk_profile_user_desa');            
            // $table->foreign('desa_id', 'fk_profile_user_desa')
            //     ->references('id')
            //     ->on('desa')
            //     ->onDelete('set null')
            //     ->onUpdate('cascade');                    



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_users');
    }
}
