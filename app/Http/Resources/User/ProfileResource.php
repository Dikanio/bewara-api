<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Master\LocationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {        
        return [
            'nik' => $this->nik,
            'no_kk' => $this->no_kk,
            'location' => new \ArrayObject([
                'address' => $this->address,
                'provinsi' => new LocationResource($this->provinsi),
                'kota' => new LocationResource($this->kota),
                'kecamatan' => new LocationResource($this->kecamatan),
                'desa' => new LocationResource($this->desa),
                'rt' => $this->rt,
                'rw' => $this->rw
            ]),
            'phone_number' => $this->phone_number,
            'file' => null
        ];
    }
}
