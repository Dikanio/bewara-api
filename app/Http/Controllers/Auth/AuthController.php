<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Models\Role;
use App\Models\PasswordReset;
use DB;

class AuthController extends Controller
{   
    public function resetPassword($token)
    {
        return view('auth.reset-password', [
            'token' => $token
        ]);
    }

    public function resetPasswordStore(Request $request, $token)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [            
                'password' => 'required|string|confirmed',
            ]);
    
            if ( $validator->fails() ) return redirect()->back()->withErrors($validator);
    
            $passwordReset = PasswordReset::where('token', $token)->first();
    
            if ( empty($passwordReset) ) return redirect()->back()->withErrors(['password' => __('Failed')]);
    
            $user = User::where('email', $passwordReset->email)->first();
            $user->password = Hash::make($request->password);            

            if ($user->save()) {
                $passwordReset->delete();
                DB::commit();
            }
            
            return redirect()->back()->with(['status' => __('Success')]);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with(['status' => __('Failed')]);
        }                    
    }
}
