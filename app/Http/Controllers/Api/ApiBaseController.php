<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ApiBaseController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($code = 200, $message = null, $data = null, $pagination = false)
    {
        if (!$pagination) {
            return response()->json([
                'meta' => new \ArrayObject([
                    'code' => $code,
                    'message' => $message
                ]),
                'data' => $data,
            ]);
        }

        $data['meta']['code'] = $code;
        $data['meta']['message'] = $message;
        
        return response()->json([
            'data' => $data['data'],
            'meta' => new \ArrayObject($data['meta']),
        ]);
    }
}
