<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Models\Role;
use App\Models\ProfileUser;
use App\Models\PasswordReset;
use App\Mails\ResetPasswordRequest;
use App\Mails\VerifyEmail;
use Carbon\Carbon;
use DB;

class AuthController extends ApiBaseController
{   
    protected $code, $message, $data;

    public function __construct() {
        $this->code = config('constants.CODE.SUCCESS');
        $this->message = __('general.success');
        $this->data = null;
    }

    public function login(Request $request)
    {   
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'bail|required',
                'password' => 'required',
            ]);
    
            if ($validator->fails()) {
                $this->code = config('constants.CODE.UNPROCESSABLE');
                $this->message = $validator->errors()->first();                
            } else {
                if (Auth::attempt(
                    [
                        'username' => $request->username, 
                        'password' => $request->password,
                    ]
                )) { 
                    $user = Auth::user(); 
                    $this->data =  new \ArrayObject([
                        'token' => $user->createToken(config('app.name'))->accessToken,
                        'user' => new UserResource($user),
                    ]);                
                    $this->message = __('general.success');
                } else { 
                    $this->code = config('constants.CODE.FAILED');
                    $this->message = __('general.failed');                    
                }                 
            }            

            return $this->response($this->code, $this->message, $this->data);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }

    public function logout(Request $request)
    {
        $this->message = __('general.failed');
        try {
            $user = Auth::user();            
            if ( $user->token()->delete() ) {
                $this->message = __('general.success');
            }            
            return $this->response($this->code, $this->message, $this->data);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }        
    }

    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'nik' => 'bail|required',
                'no_kk' => 'bail|required',
                'name' => 'bail|required',
                'username' => 'bail|required|unique:users',
                'email' => 'bail|required|email|unique:users',
                'password' => 'bail|required',
                'phone_number' => 'bail|required',
                'file' => 'bail|nullable',
                'address' => 'bail|required',
                'provinsi_id' => 'bail|required',
                'kota_id' => 'bail|required',
                'kecamatan_id' => 'bail|required',
                'desa_id' => 'bail|required',
                'rw' => 'bail|required',
                'rt' => 'bail|required',
            ]);
    
            if ($validator->fails()) {
                $this->code = config('constants.CODE.UNPROCESSABLE');
                $this->message = $validator->errors()->first();  
            } else {
                $user = new User();
                $user->role = User::DEFAULT_ROLE['RT'];
                $user->name = $request->name;
                $user->email = $request->email;
                $user->username = $request->username;
                $user->password = Hash::make($request->password);  
                if ( $user->save() ) { 
                    $profile = new ProfileUser();
                    $profile->user_id = $user->id;
                    $profile->provinsi_id = $request->provinsi_id;
                    $profile->kota_id = $request->kota_id;
                    $profile->kecamatan_id = $request->kecamatan_id;
                    $profile->desa_id = $request->desa_id;
                    $profile->nik = $request->nik;
                    $profile->no_kk = $request->no_kk;
                    $profile->address = $request->address;
                    $profile->rt = $request->rt;
                    $profile->rw = $request->rw;
                    $profile->phone_number = $request->phone_number;
                    if ( $request->has('file') ) {
                        $file = $request->file('file');
                        $filename = 'file-'.$user->id.'-'.Carbon::now()->format('dmY').'.'.$file->getClientOriginalExtension();
                        
                        $file->storeAs('file/', $filename);
                        $profile->file = $filename;
                    }                    
                    $profile->save();
                    // $encrypt = Crypt::encryptString($user->id);
                    // Mail::to($request->email)->send(new VerifyEmail($encrypt));
                                    
                    $this->message = __('general.success');
                    DB::commit();
                } else { 
                    $this->code = config('constants.CODE.FAILED');
                    $this->message = __('general.failed');                    
                }                 
            }            

            return $this->response($this->code, $this->message, $this->data);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }
    
    public function forgotPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [            
                'email' => 'required',
            ]);
    
            if ($validator->fails()) {
                $this->code = config('constants.CODE.UNPROCESSABLE');
                $this->message = $validator->errors()->first();                
            } else {
                $user = User::where('email', $request->email)->first();
                $passwordReset = null;
                if ( empty($user) ) {
                    $this->code = config('constants.CODE.FAILED');
                    $this->message = "Not Found";
                } else {
                    $passwordReset = PasswordReset::where('email', $user->email)->first();
                    if (!$passwordReset) {
                        $passwordReset = new PasswordReset();
                    }
                    $passwordReset->email = $user->email;
                    $passwordReset->token = Str::random(60);
                    $passwordReset->created_at = Carbon::now();
                    $passwordReset->save();
                    DB::commit();
                }

                if ( $user && $passwordReset ) {                    
                    Mail::to($user->email)->send(new ResetPasswordRequest($passwordReset->token));
                }                                                            
            }                
            
            return $this->response($this->code, $this->message, $this->data);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response($this->code, $e->getMessage(), $this->data);
        }                                      
    }

    public function username()
    {
        return 'username';
    }
}
