<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Resources\Master\LocationResource;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\Desa;
use Carbon\Carbon;
use DB;

class MasterController extends ApiBaseController
{   
    protected $code, $message, $data;

    public function __construct() {
        $this->code = config('constants.CODE.SUCCESS');
        $this->message = __('general.success');
        $this->data = null;
    }

    public function getProvinsi(Request $request)
    {
        try {
            $data = Provinsi::when($request->provinsi_id ?? false, function ($query, $id) {                
                    $query->where('id', $id);
                })->paginate(10);

            $this->data =  LocationResource::collection($data)->response()->getData(true);
                
            return $this->response($this->code, $this->message, $this->data, true);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }

    public function getKota(Request $request)
    {
        try {
            $data = Kota::when($request->provinsi_id ?? false, function ($query, $id) {                
                    $query->where('provinsi_id', $id);
                })
                ->when($request->kota_id ?? false, function ($query, $id) {                
                    $query->where('id', $id);
                })
                ->paginate(10);
            $this->data =  LocationResource::collection($data)->response()->getData(true);
                
            return $this->response($this->code, $this->message, $this->data, true);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }

    public function getKecamatan(Request $request)
    {
        try {
            $data = Kecamatan::when($request->kota_id ?? false, function ($query, $id) {                
                    $query->where('kota_id', $id);
                })
                ->when($request->kecamatan_id ?? false, function ($query, $id) {                
                    $query->where('id', $id);
                })
                ->paginate(10);
            $this->data =  LocationResource::collection($data)->response()->getData(true);
            
            return $this->response($this->code, $this->message, $this->data, true);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }

    public function getDesa(Request $request)
    {
        try {
            $data = Desa::when($request->kecamatan_id ?? false, function ($query, $id) {                
                    $query->where('kecamatan_id', $id);
                })
                ->when($request->desa_id ?? false, function ($query, $id) {                
                    $query->where('id', $id);
                })
                ->paginate(10);
            $this->data =  LocationResource::collection($data)->response()->getData(true);
                
            return $this->response($this->code, $this->message, $this->data, true);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }

}