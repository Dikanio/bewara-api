<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\ApiBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\UserFcm;
use DB;

class UserController extends ApiBaseController
{   
    protected $code, $message, $data;

    public function __construct() 
    {
        $this->code = config('constants.CODE.SUCCESS');
        $this->message = __('general.SUCCESS');
        $this->data = null;
    }

    public function show($id)
    {
        try {
            $user = Auth::user();
            $this->data =  new \ArrayObject([
                'user' => new UserResource($user),
            ]);
            return $this->response($this->code, $this->message, $this->data);
        } catch (\Exception $e) {
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::find($id);

            if (!$user) return $this->response(config('constants.CODE.SUCCESS'), 'Not Found', $this->data);

            $validator = Validator::make($request->all(), [
                'nik' => 'bail|required',
                'no_kk' => 'bail|required',
                'name' => 'bail|required',
                'username' => 'bail|required|unique:users,username,'.$user_id,
                'password' => 'bail|nullable',
                'phone_number' => 'bail|required',
                'address' => 'bail|required',
                // 'provinsi_id' => 'bail|required',
                // 'kota_id' => 'bail|required',
                // 'kecamatan_id' => 'bail|required',
                // 'desa_id' => 'bail|required',
            ]);
    
            if ($validator->fails()) {
                $this->code = config('constants.CODE.UNPROCESSABLE');
                $this->message = $validator->errors()->first();  
            } else {
                $user = Auth::user();                                 
                $user->name = $request->name;
                $user->username = $request->username;
                
                if($request->filled('password')) $user->password = $request->password;

                $profile = $user->profile;
                $profile->nik = $request->nik;
                $profile->no_kk = $request->no_kk;
                $profile->phone_number = $request->phone_number;
                $profile->address = $request->address;
                // if ( $request->has('photo') ) {
                //     $file = $request->file('photo');
                //     if ($profile->photo != null) {
                //         Storage::delete(config('constants.STORAGE_PATH.PHOTO_PROFILE').'/'.$profile->photo);
                //     }
                //     $filename = 'photo-'.$user->id.'-'.Carbon::now()->format('dmY').'.'.$file->getClientOriginalExtension();
                    
                //     $file->storeAs(config('constants.STORAGE_PATH.PHOTO_PROFILE'), $filename);
                //     $profile->photo = $filename;
                // }                    
                $profile->save();                
                                
                $this->message = __('general.SUCCESS');

                DB::commit();               
            }            

            return $this->response($this->code, $this->message, $this->data);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(config('constants.CODE.SERVER_FAILED'), $e->getMessage());
        }
    }
}
