<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var array 
     */
    protected $table = 'desa';

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id')->withDefault(new Kecamatan());
    }
}
