<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var array 
     */
    protected $table = 'provinsi';
}
