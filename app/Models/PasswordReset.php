<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $primaryKey = 'email';
    public $incrementing = false;
    
    protected $fillable = [
        'email', 'token', 'created_at'
    ];
}
