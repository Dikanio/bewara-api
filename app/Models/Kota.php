<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var array 
     */
    protected $table = 'kota';

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_id')->withDefault(new Provinsi());
    }
}
