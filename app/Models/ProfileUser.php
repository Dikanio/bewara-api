<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileUser extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_id')->withDefault(new Provinsi());
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id')->withDefault(new Kota());
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id')->withDefault(new Kecamatan());
    }


    public function desa()
    {
        return $this->belongsTo(Desa::class, 'desa_id')->withDefault(new Desa());
    }

}
