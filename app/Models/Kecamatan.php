<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var array 
     */
    protected $table = 'kecamatan';

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id')->withDefault(new Kota());
    }
}
